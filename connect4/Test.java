package connect4;

// Test.java
import java.util.List;

public class Test {
    private Connect4Game board;

    public Test() {
        System.out.println("Hello, World!");

        board.play('o', 1);
        board.play('x', 1);
        board.play('o', 2);
        board.play('x', 2);
        board.play('x', 3);
        board.play('x', 3);

        System.out.println("board:");
        System.out.println(board);
        System.out.println("****************");
        System.out.println("is win for x? " + board.isWin('x'));
        System.out.println("****************");

        List<Connect4Game> next = board.allNextMoves('x');
        int i = 1;

        for (Connect4Game b : next) {
            System.out.println(b);
            System.out.println("is win for x? ");
            i++;
        }
    }
}
