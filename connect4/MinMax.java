package connect4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class MinMax {
    static List<Object> maxMove(Connect4Game current, int alpha, int beta, int depth) {
        char computer = 'o';

        if (depth == 0 || current.isFinished()) {
            return new ArrayList<>(Arrays.asList(current.evaluate(computer), current));
        }

        int maxMove = Integer.MIN_VALUE;
        Connect4Game bestMoveState = null;
        char turn2=Constant.COMPUTER;
        for (Connect4Game nextState : current.allNextMoves(computer)) {
            List<Object> eval = minMove(nextState, alpha, beta, depth - 1);

            if (maxMove < (int) eval.get(0)) {
                maxMove = (int) eval.get(0);
                bestMoveState = nextState;
            }

            alpha = Math.max(maxMove, alpha);
            if (beta <= alpha) {
                break;
            }
        }
        List<Object> result = new ArrayList<>();
        result.add(maxMove);
        result.add(bestMoveState);
        return result;
    }

    private static List<Object> minMove(Connect4Game current, int alpha, int beta, int depth) {
        if (depth == 0 || current.isFinished()) {
            return new ArrayList<>(Arrays.asList(current.evaluate(Constant.HUMAN), current));
        }

        int minMove = Integer.MAX_VALUE;
        Connect4Game bestMoveState = null;

        for (Connect4Game nextState : current.allNextMoves(Constant.HUMAN)) {
            List<Object> eval = maxMove(nextState, alpha, beta, depth - 1);

            if (minMove > (int) eval.get(0)) {
                minMove = (int) eval.get(0);
                bestMoveState = nextState;
            }

            beta = Math.min(beta, minMove);
            if (alpha >= beta) {
                break;
            }
        }

        List<Object> result = new ArrayList<>();
        result.add(minMove);
        result.add(bestMoveState);
        return result;
    }
}
