package connect4;

public class Constant {
    public static final char COMPUTER = 'o';
    public static final char HUMAN = 'x';
}
